| command       | Action   | 
|---------------|-----------|
| tmux new -s <mysession>        | create a neu session |  
| tmux ls         |        list all sessions   |
| tmux a -t <mysession> |     start the session      | 
| Ctrl + b   d         |      Detach from session     | 
