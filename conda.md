| command       | Action   | 
|---------------|-----------|
| conda env list        | List availibele env |  
| conda list         |        list pkg in active env   |
| conda create -n rStudioServer  |     create env      | 
| conda install -n rStudioServer -c channel pkg         |      install pkg into env     | 
| conda activate rStudioServer        |      start env     | 
