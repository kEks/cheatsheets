# Vim

| General                           |                                 |
|-----------------------------------|---------------------------------|
| Close                      | :q                     |
| write/Save                 | :w                              | 
| force close                       | :q!                             | 
| save and close      | :wq                              | 
| undo | u |
| redo | ctrl + r |

| Curser Navigate                           |                                 |
|-----------------------------------|---------------------------------|
| Right                     | l                    |
| Left                  | h                              | 
| Down                        | j                              | 
| Up      | k                              | 
| next word       | w                              | 
| previous word      | b                             | 
| jump to line end      | $                              | 
| jump to line start     | 0                              | 
| jump to line #     | #G                              | 
| jump to End of File     | G                              | 
| jump to beginning of File     | gg                              | 
| jump to matching character     | %                              | 
| jump tp next _ in same line (_ any character)     | f_                              | 

| Page Navigate                           |                                 |
|-----------------------------------|---------------------------------|
| move screen to center of cursor                     |zz                    |
| page up                    |ctl+f                    |
| page down                    |ctrl+f                    |

| Folding                           |                                 |
|-----------------------------------|---------------------------------|
| Alles Falten                      | \rf oder zM                     |
| Open all Folders                  | zR                              | 
| Auf und Zu                        | za                              | 
| Auf und Zu  (ink unterfolds)      | zA                              | 

| Textmaipulation within normal mode                           |                                 |
|-----------------------------------|---------------------------------|
| delete inner word                       |     diw               |
| delete line                 | dd                              | 
| delete line content               | 0D                              |    
| delete character                       | x                              | 

| Editing                           |                                 |
|-----------------------------------|---------------------------------|
| replace a singe character                      |     r               |
| replace a line                | cc                              | 
| change inner word                       | ciw                              | 

| Navigate and switch to insert                           |                                 |
|-----------------------------------|---------------------------------|
| Insermode befor curser                     |     r               |
|  Insermode after curser                    | i                              | 
|  Insermode at line start                   | a                              | 
|  Insermode at line end                     | I                              | 
|  Insermode befor curser                    | A                              | 
|  Insermode in line below                   | o                              | 
|  Insermode in line above                   | O                              | 

| COPY/PASTE                        |                                 | 
|-----------------------------------|---------------------------------|
| makieren                          | v                               |
| Copy                              | y                               |
| move                              | d                               |
| paste vorCurser                   | P                               |
| paste nachCurser                  | p                               |

|    Latex                          |        only in VIMLatexSuite                         |    
|-----------------------------------|---------------------------------|
| compile                           | \ll                             | 
| view                              | \lv                             |  
| curser to Prev                    | STRG-o                          | 


| Various                      |                                 | 
|-----------------------------------|---------------------------------|
| autocomplete                      | strg+ n                         |
| envirometn                        | F5                              |
| jump placeholder                  | strg+j                          |
| open new tab                      | strg+W  T                       |
|find _                           | /_                              |  
|undo                          | u                              |    
|redo                           | ctrl-r                              | 


| Spell                             |  my vimrc                               | general   |
|-----------------------------------|---------------------------------|----|
| next word                         | Right                           | ]s |
| pre word                          | Left                            | [s |
| wordlist                          | Up                              | z= |
| ins spellfile aufnehemen          | Down                            | zg |
| temp aufnehmen                    | zG                              | zG |
| Sprache Deutsch                   | :setlocal spell spelllang=de_de |    |