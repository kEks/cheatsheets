# Org-Noter

On my Mac:  
⌘ -> ⌘  
M -> ctrl

| keys          | Command   | Acion        |
|---------------|-----------|--------------|
| M + x         | org-noter |              |
| M + i         |           | precise Note |
| ⌘ + x | ⌘ + x |           | Taq          |
| M + .         |           | jump to note |
| SPACE         |           | scroll down  |