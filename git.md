| command       | Action   | 
|---------------|-----------|
| git branch -a       | list all branches |  
| git branch -d *NAME* |     delete local branch       | 
| git fetch --prune         |        remove local branch that no longer exist remote   |
| git checkout -b *NAME* |     creat new branch      | 
| git checkout *NAME* |     swtich branch      | 
