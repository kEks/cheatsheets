# Mac
| keys   | Action                 |
|-------|------------------------|
| ⌘⇧N   | New Folder             |
| ⌘⇧N   | Folder UP            |
| ⌘⌥v   | cut and paste          |
| ⌘⇧⌥   | Paste without Formating     |
| ⌥M    | Tilde                  |
|  ⌘⇧7  | Blackslash            |
| ⌘⇧S   | Save As                |
| ⌘Q    | Quit                   |
| ⌘⌥Q   | Force Quit             |
| ⌘K    | "Mit Server Verbinden" |
| ⌘⌥ESC | Open Task Manager      |